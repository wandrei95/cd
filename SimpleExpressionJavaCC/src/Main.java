import java.io.File;
import java.io.FileInputStream;

import foo.*;
import visit.*;

public class Main {

	public static void main(String[] args) {
		try {
			ExpressionParser p = new ExpressionParser(new FileInputStream(new File("./samples/lab03.txt")));
			ASTStart root = p.Start();
			PolishPrefixVisitor ppVisitor = new PolishPrefixVisitor();
			root.jjtAccept(ppVisitor, null);
			PolishPostfixVisitor ppostVisitor = new PolishPostfixVisitor();
			root.jjtAccept(ppostVisitor, null);
			NormalVisitor normalVisitor = new NormalVisitor();
			root.jjtAccept(normalVisitor, null);
			EvaluateVisitor evalVisitor = new EvaluateVisitor();
			root.jjtAccept(evalVisitor, null);
			System.out.println("Thank you.");
		} catch (Exception e) {
			System.err.println("Oops.");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
