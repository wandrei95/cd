package main;

import java.io.*;
import mainframe.MainFrame;
import minijavaparser.*;
import visit.ClassTable;
import visit.SymbolTableVerifier;
import visit.SymbolTableVisitor;
import visit.TypeTable;

public class MainClass 
{
	public static void main(String args[]) 
	{
	    System.out.println("Reading from standard input...");
	    try 
	    {
	      	MiniJava p = new MiniJava(new FileInputStream(new File("./samples/test01.java")));
	    	ASTProgram root = p.Program();

			SymbolTableVisitor visitor = new SymbolTableVisitor();
			root.jjtAccept(visitor, null);
			visitor.print();
			
			ClassTable classTable=visitor.getClassTable();
			TypeTable typeTable=visitor.getTypeTable();
			SymbolTableVerifier verifier = new SymbolTableVerifier(typeTable,classTable);
			root.jjtAccept(verifier, null);
			verifier.print();
	    	
			MainFrame frame=new MainFrame(root);
			frame.setVisible(true);			
			
			System.out.println("Thank you.");
	    } 
	    catch(Exception e)
	    {									      
			System.err.println("Oops.");
			System.err.println(e.getMessage());
			e.printStackTrace();
	    }
	}
}
