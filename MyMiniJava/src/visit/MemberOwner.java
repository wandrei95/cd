package visit;

import visit.MemberTable.MemberReference;

public interface MemberOwner {

	public void addMember(MemberReference member);
}
